#    DevOps Essentials

### Infrastruture as code (IAC)

Describe una sintaxis de alto nivel que permite planificar tu datacenter y
versionar el mismo al igual que se hace con cualquier otro código. Aparte de
que la Infraestructura puede ser re-usada y/o compartida. Es una herramienta
OpenSource y escrita en GO.

**Ventajas:**

1. Automatización del proceso de aprovisionamiento y despliegue. Con lo cual se
hace más rápido y confiable el proceso.
1. El estado de la Infraestructura reposa en archivos de origen y configuración
que cualquiera puede leer, modificar y compartir y no en la cabeza del sysadmin
1. Los configs files pueden estar en CSVs y todo el historuial de la
infraestructura en un registro de commits.
1. Validar los cambios, a traves de revisiones de código y pruebas
automatizadas.
1. Puede crear, vender y/o comprar bibliotecas de código de infraestructura,
reutilizable, documentada y probada.

### Execution Plans

TerraForm es una herramienta de planificación que permite generar un plan de
ejecución, que muestra que se hará cuando se aplique. Esto evita sorpresas
inesperadas al manipular la infraestructura.

### Gráfico de recursos

TerraForm construye un gráfico de todos los recursos y paraleliza la creación y
modificación de cualquier recurso no dependiente.

### HashiCorp Configuration Language (HCL)

Es un lenguaje creado especificamente para el tema de configuración de
infraestructura, compatible con JSON, lo que lo hace interoperable. referencia
sobre HCL [Aquí](https://www.linode.com/docs/applications/configuration-management/introduction-to-hcl/)

### Other similar tools

* Chef
* Puppets
* Boto, Fog
* CloudFormation


Terraform se puede utilizar para orquestar cluster de servidores en AWS y
OpenStack al mismo tiempo.

### Examples links

*  [Examples templates to Terraform script](https://github.com/terraform-providers/terraform-provider-aws/tree/master/examples)
*  [A introduction to terraform code](https://github.com/gruntwork-io/intro-to-terraform)
*  [A Comprehensive guide to Terraform](https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca)
*  [demystified Terraform](https://medium.com/faun/terraform-remote-backend-demystified-cb4132b95057)
*  [Code Example of Terraform Up & Running Oreally](https://github.com/brikis98/terraform-up-and-running-code)

### AWS IAM

Es el servicio de gestión de recursos y acceso de usuarios a los mismos.

**Algunas políticas de acceso:**

1. Mejor definirlos por escrito y configurarles vía **IaC** ejemplo con Terraform.
1. Menor cantidad de derechos posibles.  Cuantos menos permisos otorgue, mejor.
    Esto previene ataques y/o errores.
1. Creación de **roles** para agrupar permisos.

### Copy files to aws over scp

~~~
scp nom_arch -i "key_name.pem" user@amazon_id.amazonaws.com:/dest_path
~~~

![Credential enviroments](static/images/configure_aws_credential_env.png "Credential Enviroments")

### Enlaces de Interés

* [AWS IAM](https://medium.com/@sirech/an-overview-of-iam-in-aws-1d9cbb1b31a4 "Aws Iam")

