##############################################################################
#   VARIABLES
##############################################################################
variable "aws_key_name" {}
variable "public_key" {}


##############################################################################
#   PROVIDERS
##############################################################################

provider "aws" {
    profile = "default"
    region  = "us-east-1"
    #  region  = "us-east-1c"
}

##############################################################################
#   RESOURCES
##############################################################################

resource "aws_key_pair" "auth" {
  #  key_name   = "kp_test_access_jorge_test_03"
  #  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzlNHhRy3dGkGedayUWZp9nws+TmXRFn7YRYEzLgItjDMzLTOOqDtO9+zvhrNCr1vPm24f/kqME0MPKU3zXkI7Z/dFdMr/5/44fxKxPwA5GvTI05aR/YGkpX5ptlY8oQQPX3q1F0oPIP57asuvTVUoxEsYyGaZ5r+rGJAMTf83iH69s+Mn2pwn1Ki1z0W84SUkZzhayLVsuOKfiElb9jlQR0GHwHpzqVz+s87XjWL08WHqkw3csm060vvDQDD9WoftHrQ9xfIX7GcftrmPQMAj4v4tZG+/fa/PS08GYvFNW3ySEQWcylMiJJyjiO3fPSEGDYkrjt7vKiQIXPparjFPrZl/S94I73Q/nOLZU4Er0ENRO1M07HksHuVdizXWiTP1sJpBAehfGlgftW7NGNSIz46S/2W1S+sg1AEDbkog+RybFwkLvlmRUuauM2MGrmz0vlL1kOnHU+y2RzrFwxhhkh3NsKahQg4dWnTqnviag/2VmWYMiHtxPEMg5Tgm/eIQKrgaeEppSAkc3//UdldMjPijbia6TC3SGJHhFRT+xfi43gGxIyl1pB61WjtAf651SYludZi12GbluoDnRFDCE2a+7CinjAyy8Rs8iGk/pJRZ7B9YDep8PtxusesTFdMxP6vmWFYnsoPR4SxDcsTepb0fHV1aSU7RjrfEC8qH6Q== jescalona@quanam.com"
  key_name   = "file(var.aws_key_name)"
  public_key = "file(var.public_key)"
}

resource "aws_instance" "example" {
    ami    = "ami-026c8acd92718196b"
    instance_type = "t2.micro"
    key_name   = "kp_test_access_jorge_test_03"
    tags = {
        name = "jorge_instance_test"
    }
    # ebs_block_device {
    root_block_device {
        # device_name = "/dev/sdg"
        volume_size = 28
        iops = 100
        volume_type = "gp2"
        #  delete_on_termination = false
    }
}
