# Gitlab in DevOps strategy

![gitops](static/images/gitops_icon.png)

They are a set of tool for devops in gitlab. First is necessary understand a few
concepts of gitlab infrastructure, like next:

### Gitlab glosary

* **gitops**: its the practice of use the git repository like the center of all
infrastructure and application deployment code. see the [video](https://www.youtube.com/watch?time_continue=33&v=5rqoLj8N5PA&feature=emb_logo).

[![](http://img.youtube.com/vi/5rqoLj8N5PA/0.jpg)](http://www.youtube.com/watch?v=5rqoLj8N5PA "Gitlab gitops with terraform")

**Sample demo repository:**

* [demo infrastructure repository](https://gitlab.com/gitops-demo)

is very nice practice use [terraform cloud](https://www.terraform.io/) to storage the states of projects
prevents multiple jobs from making conflicting changes at the same time.

### interesting links:

* [configure token appliction between gitlab and terraform.io](https://www.terraform.io/docs/cloud/vcs/gitlab-com.html)
