# OpenShift Platform / Openshift Container Platform (OCP)

En el 2013 en el marco de la PyCon US, una compañia llamada dotCloud anunciaba
una nueva herramienta llamada **Docker.** Despliegue de contenedores sobre [LXC Linux Containers](https://es.wikipedia.org/wiki/LXC "Linux Containers")
A mediados de 2014, Google anunció el proyecto [Kubernetes](https://kubernetes.io/es/), un sistema de código
abierto para automatizar la implementación, el escalado y la administración de
aplicaciones en contenedores. **OpenShift** decide abandonar su proyecto propio de
orquestación de contenedores y adopta **Kubernets** como estandar defacto.

![Evolución de Plataformas de Servicios](../static/images/platform_evolution.png "ServicesPltaformsEvolution")

Para el uso de este ,tipo de servicios siempre es necesario suministrar la imagén
del contenedor a desplegar. Por eso **OpenShift** tiene la habilidad de desplegar
cualquier aplicación a la cuál se referencie el **registry** de su imagen.

Utilizando un servicio como GitHub, GitLab o Bitbucket, puede configurar el
servicio de alojamiento del repositorio de Git para notificar a OpenShift cada
vez que envíe cambios a su código al repositorio de Git alojado. Esta
notificación puede desencadenar una nueva compilación y despliegue de su aplicación.

Es el **CloudServices** de [RedHat](https://www.redhat.com/es "RedHat"), a niveles prácticos es un [PaaS](https://en.wikipedia.org/wiki/Platform_as_a_service "PaaS"), posee alta integración con [git](https://es.wikipedia.org/wiki/Git "Git")
Todo el código del proyecto está disponible sobre la licencia [Apache](https://es.wikipedia.org/wiki/Apache_License "Apache")

* Una parte importante es que ofrecen [CaaS](https://www.arsys.es/blog/soluciones/cloud/cloud-computing/que-es-iaas-saas-paas-faas-caas-te-explicamos-en-que-consisten-los-diferentes-servicios-de-la-nube/ "CaaS") **Container As A Service.**
Los motores de contenedores, la orquestación y los recursos informáticos subyacentes
se entregan a los usuarios como un servicio de un proveedor en la Nube.

* Ofrecen acertadamente el salto paradigmático en la transición de VMs a Containers
desde el enfoque de los beneficios operativos del cambio. Iteraciones más rápidas,
y despliegues más ágiles, entre los puntos más resaltantes.

* Otro fuerte no solo de OpenShift sino de la mayoría de los CloudServices modernos
es la escalabilidad, casi instantanea e ilimitada de los servicios que ofreces
a tus customers/clientes.

### Opciones al empezar con OpenShift

* Free Service, se ofrece a manera de prueba y con fines formativos por un
periodo de 30 días.

* OpenShift Online: alojado públicamente por RedHat. De está manera no instalas
ni administras tu cluster y dejas eso en manos de la empresa (IaaS).

* OpenShift Dedicated: Similar a Online pero el clúster de OS está reservado para
su uso y no lo comparten otros usuarios fuera de su organización (PasS).

### Video Overview OpenShit

[![OpenShift Overview](../static/images/rh_video_min.png)](https://www.youtube.com/watch?time_continue=1&v=5dwMrFxq8sU "OpenShift Overview")

### Enlaces de Interés

* [OpenShift](https://es.wikipedia.org/wiki/OpenShift "OpenShift")
* [OS+AWS+Terraform article](https://enmilocalfunciona.io/terraform-aws-openshift/)
* [Helm Kubernets Package Manager](https://helm.sh/docs/ "Helm!")

### Odoo over OpenShift

* [Add Odoo on OpenShift](https://github.com/dreispt/openshift-odoo-quickstart)
* [Add Odoo on OpenShift ii](https://github.com/debianmaster/odoo-on-openshift)


