[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

# Selenium for test Frontend

Entorno de pruebas de software para aplicaciones basadas en la web. Selenium
provee una herramienta de grabar/reproducir para crear pruebas sin usar un
lenguaje de scripting para pruebas (Selenium IDE).

**Lenguajes Soportados:** Java, C#, Ruby, Perl, Php y Python.

**Selenium:** Es un conjunto de herramientas que permiten testear en el Browser y
que son soporte principal a las puebas de integración y las pruebas E2E
(end-to-end), durante las cuales queremos hacer exactamente lo que el usuario hace
cuando clickea sobre nuestra aplicación.

![Selenium IDE](../static/images/selenium_ide_over_brave_browser.png "SeleniumIDE")

Selenium va capturando las clases de elementos en el DOM y podemos identificarlos
con nombres únicos lo que da la habilidad de reproducir los **test** funcionales luego
invocando dichos **IDs**. Con Odoo el problema es que estos identificadores van
cambiando a medida que desplazamos el puntero del ratón a través de la pantalla.
Se a desarrollado una herramienta que permite hacer dicha identificación de
clases. La demo expuesta en el vídeo apunta a esta [herramienta](https://github.com/brain-tec/selenium-builder) para FireFox.

El renderizado de Odoo vía **QWeb** agrega otra traba al querer usar selenium que
toma los eventos del DOM desde el js mismo.

**Conclusión:** por si solo **selenium** no está adaptado para trabajar con Odoo, existen
avances en la adaptación del mismo por parte de la gente de [Brain-tec](https://www.braintec-group.com) entonces
es necesario, probar los mismos y adaptarlos a las peculiaridades de prueba que
se presenten. Tomar en consideración que debe ser una version antigua de FireFox
ya que las más modernas cambian aspectos fundamentales del motor render que no
son compatibles con está solución.

# Jenkins
![Jenkins](../static/images/jenkins_logo.png "Jenkins")

Servidor de automatización escrito en JAVA.

### interesting links:
**selenium**
* [Official Selenium site](https://www.seleniumhq.org "WebBrowser automates test")
* [Selenium in Wikipedia](https://es.wikipedia.org/wiki/Selenium "Selenium Wikipedia")
* [Odoo + Selenium](https://www.youtube.com/watch?v=jBiSME1XDpE "Odoo+Selenium")
* [Ejemplos de Selenium + Django](https://gitlab.com/jorgemustaine/tdd_goat/blob/master/functional_tests.py "Selenium+Django")
* [Brain-Tec github site](https://github.com/brain-tec "SeleniumToolsToOdoo")

**Jenkins**
* [Oficcial Jenkins site](https://jenkins.io "Jenkins Oficcial")
* [Jenkins in Wikipedia](https://es.wikipedia.org/wiki/Jenkins "JenkinsWikipedia")
* [Odoo Module v10 Jenkins jobs](https://github.com/dduarte-odoogap/ci_jenkins "OdooJenkinsModule")
