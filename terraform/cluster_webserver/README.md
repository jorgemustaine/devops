# Web Server Example

This folder contains an example [Terraform](https://www.terraform.io/) configuration that deploys a cluster of web servers 
(using [EC2](https://aws.amazon.com/ec2/) and [Auto Scaling](https://aws.amazon.com/autoscaling/)) and a load balancer
(using [ELB](https://aws.amazon.com/elasticloadbalancing/)) in an [Amazon Web Services (AWS) 
account](http://aws.amazon.com/). The load balancer listens on port 80 and returns the text "Hello, World" for the 
`/` URL.

For more info, please see Chapter 2, "Getting started with Terraform", of 
*[Terraform: Up and Running](http://www.terraformupandrunning.com)*.

# ASG (Auto Scaling Group)

Es un servicio de AWS que permite asegurar la escabilidad de sistema mediante la
administración de cluster de servidores. y con una configuración granular que
permite al equipo de infraestructura el diseño de soluciones dinámicas y
efectiva.

En nuestro ejemplo el cluster se compone de entre 2 - 10 instancias y se van
agregando contra demanada.

El parámetro de auto-scaling define la forma en que se despliegan las instancias
por ejemplo `create_before_destroy` indica que antes de deshacer una entidad
debe haber creado una nueva

### Load Balancer Configuration / Elastic Load Balancer (ELB)

Un balanceador de carga administra los request para un determinado cluster de
está forma distribuye el tráfico de red a través de los servidores a través de
un nombre **DNS**.

![ELB](../../static/images/aws_elb_00.png "ELB")


El **ELB** puede verificar periodicamente si una instancia no es saludable, detiene
entonces el enrutamiento hacia la misma, esto lo hace con el bloque `health_check`
se puede por ejemplo configurar para enviar un http request a cada una de las
instancias **EC2** y solo la considerará sana si esta responde con un `200 OK`

### enlaces de interes:

* [ASG faqs](https://aws.amazon.com/es/ec2/autoscaling/faqs/ "AWS ASG faqs")
* [Intro to Autoscaling spanish](https://docs.aws.amazon.com/es_es/autoscaling/ec2/userguide/GettingStartedTutorial.html "Intro 2 ASG")
* [Códigos de ejemplo](https://github.com/brikis98/terraform-up-and-running-code)

