# Manage Terraform

**tfstate files:** Administrar recursos de seguimiento y control de terraform sobre
la Infraestructura creada localmente es relativamente facil y seguro y bastará
con almacenar los archivos de estado `*.tfstate` en un lugar seguro.
La complejidad se manifesta toda vez que necesitamos compartir, construir y
administrar de forma descentralizada y colectiva los **IaC** files.

Como los archivos de estado, contienen información en texto simple, es mala idea
compartirlos en un repositorio **Git** ya que en él iran x ejemplo el user y secret
de una **BD**. En lugar de usar el control de versiones, la mejor manera de
administrar el almacenamiento compartido para archivos de estado es usar el
**soporte integrado de Terraform para el Almacenamiento de estado remoto.**

### Terraform remote config command.

Algunos almacenes remotos soportados:

* Amazon S3
* Azure Storage
* HashiCorp Consul
* HashiCorp Atlas

###  prevent_destroy

Configuración del ciclo de vida de algún recurso, cuando está en **true** previene
su eliminación accidental, de está forma el comando `terraform destroy` arrojará
un error. En el caso de los **cubos** S3 suele pasarse este parámetro si existe info
sencible en dicho dispositivo de almacenamiento.

Si de todas formas necesita eliminar el **bucket** (cubo S3), comente esa parte del
código ejecute: `terraform plan` y luego `terraform destroy`, eso debería ser
suficiente.

### Locking state files

[TerraGrunt](https://github.com/gruntwork-io/terragrunt "TerraGrunt")

**TerraGrunt** es un wrapper para Terraform de código abierto. Terragrunt
reenvía casi todos los comandos, argumentos y opciones directamente a Terraform,
utilizando cualquier versión de Terraform que ya haya instalado. Sin embargo,
según la configuración de su archivo terragrunt.hcl, Terragrunt puede
configurar el estado remoto, el bloqueo, los argumentos adicionales y mucho más.


