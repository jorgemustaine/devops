provider "aws" {
    region = "us-east-1"
}

resource "aws_s3_bucket" "terraform_state" {
    bucket = "terraform-running-state-jorge-warehouse"

    versioning {
        enabled = true
    }

    # Configuration of the life cycle of some resource when is set true prevents
    # accidental deletion, in this way the terraform destroy command will
    # throw an error. In the case of S3 cubes, this parameter is usually passed
    # if there is sensitive information in said storage device.


    #lifecycle {
    #    prevent_destroy = true
    #}
}

output "s3_bucket_arn" {
    value = "${aws_s3_bucket.terraform_state.arn}"
}
