#  Configure Terragrunt to use DynamoDB for locking
#lock = {
#    backend = "dynamodb"
#        config = {
#            state_file_id = "global/s3"
#        }
#}

#  Configure Terragrunt to automatically store tfstate files in S3
remote_state = {
    backend = "s3"
        config = {
            encrypt = "true"
            bucket = "terraform-running-state-jorge-warehouse"
            key = "global/s3/terraform.state"
            region = "us-east-1"
        }
}
