
### variable de entrada

en terraform la declaración de este tipo de variables puede ser como una lista
o como un **map** que sería un simíl a un diccionario en python.

![terraform variables de entrada](../../static/images/terraform_input_vars_00.png "Input Variable")

estás variables se pueden definir con **default** en el archivo tf o pasarse por
línea de comandos como sigue:

~~~
$terraform apply -var server_port="8080"
~~~

Si alguna de las anteriores se omite durante el **apply** terraform solicitará el
valor de entrada para la misma.

La mejor práctica es usar [Interpolación de Variables](https://www.terraform.io/docs/configuration-0-11/interpolation.html "variable interpolation")
Cada vez que vea un signo de dólar y llaves entre comillas dobles, eso significa
que Terraform procesará el contenido de una manera especial.

### Security

Por convención didáctica la mayoría de los ejemploa aquí expuestos usan public
IPs. En la vida real los sistemas de producción, debe implementar todos sus
servidores, y ciertamente todos sus almacenes de datos, en subredes privadas,
que tienen direcciones IP a las que solo se puede acceder desde el VPC y no de
Internet público. Los únicos servidores que deberías ejecutar en subredes
públicas son un pequeño número de servidores proxy inversos y equilibradores de
carga que bloquee tanto como sea posible.

