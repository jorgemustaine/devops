provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "jorge_test_example" {
    ami = "ami-07d0cf3af28718ef8"
    instance_type = "t2.micro"
    vpc_security_group_ids = ["${aws_security_group.instance.id}"]

    user_data = <<-EOF
                #!/bin/bash
                echo "Hello World" > index.html
                nohup busybox httpd -f -p "${var.server_port}" &
                EOF

    tags = {
        Name = "jorge_test_example_00"
    }
}

resource "aws_security_group" "instance" {
    name = "terraform_example_instance"
    ingress {
        from_port = "${var.server_port}"
        to_port = "${var.server_port}"
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

variable "server_port" {
    description = "The port the server will use fot HTTP requests"
    default = 8080
}

output "public_ip" {
    value = "${aws_instance.jorge_test_example.public_ip}"
}

