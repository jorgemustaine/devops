# Terrafrom PluralSigth

El archivo de configuración de los recursos permite definir variables que
almacenan valores que se repetiran en nuestro infraestructure_script o que no
queremos develar publicamente, como claves de acceso, tokens y otros.

![Terraform Variables](../static/images/terra_var.png)

### interesting links

* [Odoo Terraform Example](https://github.com/dagisus/oddo_terraform)

